from socket import AF_INET, SOCK_STREAM, socket
from socket import error as SocketError
from backend.message import decode_message
from backend.channel import Channel
from time import sleep
import threading


def channel_listener(ip, port, channel):
    s = socket(AF_INET, SOCK_STREAM)
    s.bind((ip, port))
    s.listen(0)
    client_socket, client_addr = s.accept()
    buffer_len = 1024

    try:
        while(1):
            msg = b''
            length_bytes = client_socket.recv(buffer_len)
            if(len(length_bytes) > 4):
                print("Got invalid message!")
                continue
            length = int.from_bytes(length_bytes, 'big')
            while len(msg) < length:
                msg += client_socket.recv(buffer_len)
            decoded = decode_message(msg)
            if decoded is not None:
                channel.add_to_inbox(decoded)
            else:
                print("Got invalid message!")
    except SocketError as e:
        print("Connection Closed!")


if __name__ == "__main__":
    channel = Channel()
    t = threading.Thread(target=channel_listener, args=('127.0.0.2', 11500, channel))
    t.start()

    while(1):
        msg = channel.get_from_inbox()
        if msg is not None:
            print(msg.time, msg.text)
        sleep(0.5)

