class Channel:
    def __init__(self):
        self.inbox = []
        self.in_ptr = 0
        self.outbox = []
        self.out_ptr = 0
        self.terminate_flag = False

    def add_to_inbox(self, msg):
        self.inbox.append(msg)

    def get_from_inbox(self):
        if self.in_ptr < len(self.inbox):
            self.in_ptr += 1
            return self.inbox[self.in_ptr-1]
        return None

    def add_to_outbox(self, msg):
        self.outbox.append(msg)

    def get_from_outbox(self):
        if self.out_ptr < len(self.outbox):
            self.out_ptr += 1
            return self.outbox[self.out_ptr-1]
        return None

    def terminate(self):
        self.terminate_flag = True