from socket import AF_INET, SOCK_STREAM, socket
from backend.message import Message, encode_message
from backend.channel import Channel
from time import sleep
import threading
import datetime


def channel_speaker(ip, port, channel):
    s = socket(AF_INET, SOCK_STREAM)
    s.connect((ip, port))

    while(1):
        msg = channel.get_from_outbox()
        if msg is not None:
            code = encode_message(msg)
            code = b"sadfasdgrqgfdsabrehwthgrq"
            s.send(len(code).to_bytes(4, 'big'))
            s.sendall(code)
        sleep(0.5)


if __name__ == "__main__":
    channel = Channel()
    t = threading.Thread(target=channel_speaker, args=('127.0.0.2', 11500, channel))
    t.start()

    while(1):
        text = input()
        msg = Message(datetime.datetime.now(), text)
        channel.add_to_outbox(msg)
