import pickle


class Message:
    def __init__(self, time, text):
        self.time = time
        self.text = text


def encode_message(input):
    return pickle.dumps(input, 3)


def decode_message(input):
    try:
        return pickle.loads(input)
    except (pickle.PickleError):
        return None

