import base64
from Crypto.Cipher import AES
from Crypto import Random


class Encryption(object):
    def __init__(self, key):
        self.key = key

    def pad(s):
        string = s + (16-len(s)%16) * chr(16-len(s)%16).encode('utf8')
        return string

    def unpad(s):
        bytes = s[:-ord(s[len(s)-1:])]
        return bytes.decode('utf8')

    def encrypt(self, plaintext):
        iv = Random.new().read(AES.block_size)
        padded_plaintext = Encryption.pad(plaintext.encode('utf8'))
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(padded_plaintext))

    def decrypt(self, ciphertext):
        ciphertext = base64.b64decode(ciphertext.decode('utf8'))
        cipher = AES.new(self.key, AES.MODE_CBC, ciphertext[:16])
        return Encryption.unpad(cipher.decrypt(ciphertext[16:]))

