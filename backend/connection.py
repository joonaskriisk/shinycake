from backend.channel import Channel
from backend.encryption import Encryption
from backend.listener import channel_listener
from backend.message import Message
from backend.speaker import channel_speaker
from backend.key_exchange import KeyExchange
from socket import AF_INET, SOCK_DGRAM, socket
from backend.checker import is_ip, is_port
import threading
import datetime
from time import sleep


class Connection:
    def __init__(self, src_ip, src_port):
        self.name = None
        self.ip = None
        self.port = None
        self.key = None
        self.channel = Channel()
        self.listener_thread = threading.Thread(target=channel_listener, args=(src_ip, src_port, self.channel))
        self.speaker_thread = None

        self.listener_thread.start()

    def connect(self, dest_name, dest_ip, dest_port, shared_key):
        self.name = dest_name
        self.ip = dest_ip
        self.port = dest_port
        self.key = shared_key
        self.speaker_thread = threading.Thread(target=channel_speaker, args=(dest_ip, dest_port, self.channel))

        self.speaker_thread.start()


def connection_listener(name, ip, port, connection_handler):
    channel_port = port+1
    s = socket(AF_INET, SOCK_DGRAM)
    s.bind((ip, port))
    buffer_len = 2048

    while(1):
        if(connection_handler.terminate_flag):
            return
        msg, src = s.recvfrom(buffer_len)

        # Initiate key exchange
        key_exchange = KeyExchange()

        # Share port with the other party
        data = msg.decode('utf8').split(';')
        if len(data) != 4 or not is_ip(data[1]) or not is_port(data[2]):
            continue

        dest_name, dest_ip, dest_port, dest_public_key = data
        dest_port = int(dest_port)
        dest_public_key = int(dest_public_key)

        connection = Connection(ip, channel_port)
        s.sendto(';'.join((name, ip, str(channel_port), str(key_exchange.get_public_key()))).encode('utf8'), src)

        # Start the channel
        connection.connect(dest_name, dest_ip, dest_port, key_exchange.set_shared_key(int(dest_public_key)))
        connection_handler.add(connection)

        channel_port += 2


def initiate_connection(name, ip, port, mng_ip, mng_port, connection_handler):
    s = socket(AF_INET, SOCK_DGRAM)
    buffer_len = 2048
    connection = Connection(ip, port)

    # Initiate key exchange
    key_exchange = KeyExchange()

    # Share port with the other party
    s.sendto(';'.join((name, ip, str(port), str(key_exchange.get_public_key()))).encode('utf8'), (mng_ip, mng_port))
    msg, src = s.recvfrom(buffer_len)

    dest_name, dest_ip, dest_port, dest_public_key = msg.decode('utf8').split(';')
    dest_port = int(dest_port)
    dest_public_key = int(dest_public_key)

    # Start the channel
    connection.connect(dest_name, dest_ip, dest_port, key_exchange.set_shared_key(int(dest_public_key)))
    connection_handler.add(connection)


class ConnectionHandler:
    def __init__(self):
        self.connections = []
        self.lock = threading.Lock()
        self.terminate_flag = False

    def add(self, connection):
        self.lock.acquire()
        self.connections.append(connection)
        print("Connection with ", connection.name, " from ", connection.ip, " initiated at connection index ", len(self.connections)-1)
        self.lock.release()

    def terminate(self):
        self.terminate_flag = True


def inbox_reader(connection_handler, archive):
    connections = connection_handler.connections
    while(1):
        length = len(connections)
        while len(archive) < length:
            archive.append([])
        for i in range(len(connections)):
            while(1):
                msg = connections[i].channel.get_from_inbox()
                if msg is None:
                    break

                decryption_algorithm = Encryption(connections[i].key)
                decrypted_message = decryption_algorithm.decrypt(msg.text)

                print(connections[i].name, ' -- ', msg.time, ' -- ', decrypted_message)

                archive[i].append(msg)
        sleep(0.5)


class ConnectionKeys(object):
    def __init__(self):
        self.dictionary = {}

    def set_key(self, dest_ip, dest_port, key):
        self.dictionary.update({(dest_ip, dest_port): key})


if __name__ == "__main__":
    connection_handler = ConnectionHandler()
    archive = []

    connection_keys = ConnectionKeys()

    print("Enter name and IP")
    name = input()
    ip = input()

    initiate_port = 11502
    listener_thread = threading.Thread(target=connection_listener, args=(name, ip, 11500, connection_handler))
    listener_thread.start()
    reader_thread = threading.Thread(target=inbox_reader, args=(connection_handler, archive))
    reader_thread.start()

    print("Initiate connection with: initiate [ip]")
    print("Send a message with: message [connection_index] [message]")
    print("Print new messages from connection with: inbox [connection_index]")

    while(1):
        cmd, args = input().split(' ', 1)

        if cmd == 'initiate':
            initiate_connection(name, ip, initiate_port, args, 11500, connection_handler)
            initiate_port += 2

        elif cmd == 'message':
            idx, msg = args.split(' ', 1)
            idx = int(idx)

            # Encrypt message
            encryption_algorithm = Encryption(connection_handler.connections[idx].key)
            encrypted_message = encryption_algorithm.encrypt(msg)

            connection_handler.connections[idx].channel.add_to_outbox(Message(datetime.datetime.now(), encrypted_message))
            print("Message Sent!")

        elif cmd == 'inbox':
            print("The messages in that inbox are:")
            idx = int(args)

            for msg in archive[idx]:

                # Decrypt message
                decryption_algorithm = Encryption(connection_handler.connections[idx].key)
                decrypted_message = decryption_algorithm.decrypt(msg.text)

                print(connection_handler.connections[idx].name, ' -- ', msg.time, ' -- ', decrypted_message)

        else:
            print("Invalid Command!")
