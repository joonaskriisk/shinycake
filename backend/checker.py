import re


def is_ip(string):
    if re.fullmatch(r"[0-9]{1,3}(\.[0-9]{1,3}){3}", string) is not None:
        nums = map(lambda x: int(x), string.split('.'))
        for num in nums:
            if num < 0 or num > 255:
                return False
        return True
    return False


def is_port(string):
    if re.fullmatch(r"[0-9]{1,5}", string) is not None:
        num = int(string)
        if num > 65535:
            return False
        return True
    return False
