import unittest
import shinycake.core.ConversationContext as con_Con
from backend.connection import Connection
#import sys
#import os
#import time


class TestConversationContext(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        # Should find a working solution to solve testing cases where method contains "while(...)"
        # os._exit(0) works closes, but stops all tests as well.
        # Current solution - run all tests, then stop all processes from PyCharm
        #time.sleep(0.5)
        #os._exit(0)
        pass

    def setUp(self):
        self.conversation_context = con_Con.ConversationContext('0.0.0.0', 'Test contact',
                                                                Connection('0.0.0.0', 11500))

    def tearDown(self):
        #with self.assertRaises(SystemExit):
         #   sys.exit(0)
        pass

    # Everything in one test, because of multiple socket address usage
    # and no way to stop previous process
    def test_conversation_context(self):
        self.assertEqual(self.conversation_context.get_contact_name(), 'Test contact')
        self.assertEqual(self.conversation_context.get_ip_address(), '0.0.0.0')
        self.assertEqual(len(self.conversation_context.get_message_list()), 0)
        self.conversation_context.add_message("Test message")
        self.assertTrue(len(self.conversation_context.get_message_list()), 1)
        self.assertEqual(self.conversation_context.get_message_list()[0], "Test message")
