import unittest
import backend.message as msg


class TestMessage(unittest.TestCase):

    def test_encode_and_decode_message(self):
        self.assertEqual(msg.decode_message(msg.encode_message("Test message")), "Test message")
        self.assertEqual(msg.decode_message(msg.encode_message("Another test message")), "Another test message")
        self.assertIsNone(msg.decode_message("Test".encode()))
