import unittest
import backend.encryption as enc
from Crypto import Random


class TestEncryption(unittest.TestCase):

    def test_encrypt_and_decrypt(self):
        aes = enc.Encryption(Random.get_random_bytes(32))
        self.assertEqual(aes.decrypt(aes.encrypt("Test message")), "Test message")
        self.assertEqual(aes.decrypt(aes.encrypt("Another test message")), "Another test message")
