import unittest
import shinycake.core.UserData as userData


class TestUserData(unittest.TestCase):

    def setUp(self):
        self.user_data = userData.UserData("Test name", "0.0.0.0")

    def test_get_name(self):
        self.assertEqual(self.user_data.get_name(), "Test name")

    def test_get_ip(self):
        self.assertEqual(self.user_data.get_ip(), "0.0.0.0")
