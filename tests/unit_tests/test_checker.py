import unittest
import backend.checker as ch


class TestChecker(unittest.TestCase):

    def test_is_ip(self):
        self.assertTrue(ch.is_ip('127.0.0.1'))
        self.assertFalse(ch.is_ip('127.0.0'))
        self.assertFalse(ch.is_ip('127.256.0.1'))

    def test_is_port(self):
        self.assertTrue(ch.is_port('11500'))
        self.assertTrue(ch.is_port('23500'))
        self.assertFalse(ch.is_port('80000'))


if __name__ == '__main__':
    unittest.main()
