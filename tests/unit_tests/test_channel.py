import unittest
import backend.channel as ch


class TestChannel(unittest.TestCase):

    def setUp(self):
        self.channel = ch.Channel()

    def test_add_to_and_get_from_inbox(self):
        self.channel.add_to_inbox("Test message")
        self.assertEqual(self.channel.get_from_inbox(), "Test message")
        self.channel.add_to_inbox("Test message")
        self.channel.add_to_inbox("Another test message")
        self.assertEqual(self.channel.get_from_inbox(), "Test message")
        self.assertEqual(self.channel.get_from_inbox(), "Another test message")

    def test_get_no_messages_from_inbox(self):
        self.assertIsNone(self.channel.get_from_inbox())

    def test_add_to_and_get_from_outbox(self):
        self.channel.add_to_outbox("Test message")
        self.assertEqual(self.channel.get_from_outbox(), "Test message")
        self.channel.add_to_outbox("Test message")
        self.channel.add_to_outbox("Another test message")
        self.assertEqual(self.channel.get_from_outbox(), "Test message")
        self.assertEqual(self.channel.get_from_outbox(), "Another test message")

    def test_get_no_messages_from_outbox(self):
        self.assertIsNone(self.channel.get_from_outbox())


if __name__ == '__main__':
    unittest.main()
