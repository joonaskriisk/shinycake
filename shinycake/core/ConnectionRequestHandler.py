from PyQt5.QtCore import QObject

from shinycake.core.ConversationContext import ConversationContext


class ConnectionRequestHandler(QObject):

    def request_connection(self, ip_address, contact_name):
        return ConversationContext(ip_address, contact_name)
