import typing

from PyQt5.QtCore import QAbstractListModel, QObject, QModelIndex, Qt, QVariant
from PyQt5.QtGui import QIcon

from shinycake.core import ConversationContext


class ContactsListModel(QAbstractListModel):

    conversations_list = []

    def __init__(self, parent: typing.Optional[QObject] = None) -> None:
        super().__init__(parent)

    def add_conversation(self, conversation: ConversationContext):
        last_row = self.rowCount()
        self.beginInsertRows(QModelIndex(), last_row, last_row)
        self.conversations_list.append(conversation)
        self.endInsertRows()

    def data(self, index: QModelIndex, role: int = ...) -> typing.Any:
        if not index.isValid():
            return QVariant()

        item = self.conversations_list[index.row()]

        if role == Qt.DisplayRole:
            return item.get_contact_name()
        elif role == Qt.DecorationRole:
            return QIcon("shinycake/ui/ic_face_3x.png")
        elif role == Qt.EditRole:
            return item

        return QVariant()

    def rowCount(self, parent: QModelIndex = ...) -> int:
        return len(self.conversations_list)

    def flags(self, index: QModelIndex) -> Qt.ItemFlags:
        return super().flags(index)
