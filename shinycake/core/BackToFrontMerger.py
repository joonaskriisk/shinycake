from time import sleep

from shinycake.core.ConversationContext import ConversationContext
from shinycake.ui.ContactsPane import ContactsPane
from shinycake.ui.ConversationPane import ConversationPane


def back_to_front_merger(connection_handler, contacts_pane: ContactsPane, conversation_pane: ConversationPane):
    connections = connection_handler.connections
    conversations = contacts_pane.contacts_list_model
    while(1):
        if(connection_handler.terminate_flag):
            return
        length = len(connections)
        while conversations.rowCount() < length:
            i = conversations.rowCount()
            contacts_pane.add_conversation(ConversationContext(connections[i].ip, connections[i].name, connections[i]))
        for i in range(len(connections)):
            updated = False
            while(1):
                msg = connections[i].channel.get_from_inbox()
                if msg is None:
                    break

                #decryption_algorithm = Encryption(connection_keys.dictionary.get((ip, initiate_port)))
                #decrypted_message = decryption_algorithm.decrypt(msg.text)
                conversations.conversations_list[i].add_message('{0:%Y-%m-%d %H:%M:%S} : {1} : {2}'.format(msg.time, connections[i].name, msg.text))
                updated = True

            if updated and conversation_pane.current_conversation is conversations.conversations_list[i]:
                contacts_pane.contact_selected.emit(conversation_pane.current_conversation)

        sleep(0.5)
