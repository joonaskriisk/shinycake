class UserData:

    def __init__(self, name: str, ip: str) -> None:
        super().__init__()
        self.name = name
        self.ip = ip

    def get_name(self) -> str:
        return self.name

    def get_ip(self) -> str:
        return self.ip

    def __str__(self) -> str:
        return f'Name {self.name} - Ip: {self.ip}'
