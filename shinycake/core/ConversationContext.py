from PyQt5.QtCore import QObject
from backend.connection import Connection


class ConversationContext(QObject):

    def __init__(self, ip_address, contact_name, connection: Connection):
        super().__init__()

        self.ip_address = ip_address
        self.contact_name = contact_name
        self.connection = connection
        self.message_list = []
        self.inbox_count = 0
        self.outbox_count = 0

    def get_contact_name(self):
        return self.contact_name

    def get_ip_address(self):
        return self.ip_address

    def get_message_list(self):
        return self.message_list

    def add_message(self, message):
        self.message_list.append(message)
