import threading

class ConnectionHandler:
    def __init__(self, my_name, my_ip):
        self.connections = []
        self.lock = threading.Lock()
        self.my_name = my_name
        self.my_ip = my_ip
        self.initiate_port = 11502
        self.terminate_flag = False

    def add(self, connection):
        self.lock.acquire()
        self.connections.append(connection)
        print("Connection with ", connection.name, " from ", connection.ip, " initiated at connection index ", len(self.connections)-1)
        self.lock.release()

    def terminate(self):
        self.terminate_flag = True
        for connection in self.connections:
            connection.terminate()
