import datetime as dt
import typing

from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import QStringListModel, QSize, pyqtSlot, pyqtSignal
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QVBoxLayout, QListView, QLabel, QStackedWidget, QWidget, QTextEdit, QSizePolicy

from shinycake.core.ConversationContext import ConversationContext

from backend.message import Message


class ConversationInput(QTextEdit):

    def __init__(self, parent: typing.Optional[QWidget] = None) -> None:
        super().__init__(parent)

        self.setFixedSize(QSize(500, 50))
        self.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

    message_entered = pyqtSignal(str)

    def keyPressEvent(self, e: QtGui.QKeyEvent) -> None:
        if e.key() == QtCore.Qt.Key_Return:
            message = self.toPlainText()
            self.message_entered.emit(message)
        else:
            super().keyPressEvent(e)


class ConversationPane(QStackedWidget):
    current_conversation: ConversationContext

    def __init__(self, connection_handler):
        super().__init__()

        self.connection_handler = connection_handler

        self.conversation_messages_model = QStringListModel()

        self.contact_name_label = QLabel()
        self.contact_name_label.setObjectName("contact_data_label")
        self.contact_name_label.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed))
        self.contact_name_label.setFixedSize(QSize(500, 50))
        self.contact_name_label.setAlignment(QtCore.Qt.AlignCenter)

        self.conversation_messages_view = QListView()
        self.conversation_messages_view.setFixedWidth(500)
        self.conversation_messages_view.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Expanding))
        self.conversation_messages_view.setModel(self.conversation_messages_model)

        self.conversation_input = ConversationInput()

        self.conversation_input.message_entered.connect(self.on_message_entered)

        conversation_layout = QVBoxLayout()
        conversation_layout.setContentsMargins(0, 0, 0, 0)
        conversation_layout.addWidget(self.contact_name_label)
        conversation_layout.addWidget(self.conversation_messages_view)
        conversation_layout.addWidget(self.conversation_input)

        conversation_widget = QWidget(self)
        conversation_widget.setLayout(conversation_layout)

        no_conversation_widget = QWidget(self)
        no_conversation_widget.setObjectName('no_conversation_widget')
        no_conversation_label = QLabel(self)
        no_conversation_image = QPixmap('shinycake/ui/default_view_image.png')
        no_conversation_label.setPixmap(no_conversation_image)
        no_conversation_label.setAlignment(QtCore.Qt.AlignCenter)
        no_conversation_layout = QVBoxLayout()
        no_conversation_layout.addWidget(no_conversation_label)
        no_conversation_widget.setLayout(no_conversation_layout)
        self.current_conversation = None

        self.addWidget(no_conversation_widget)
        self.addWidget(conversation_widget)
        self.set_default_view()
        # self.set_conversation_view()

    def on_message_entered(self, message_text):
        message = Message(dt.datetime.now(), message_text)
        self.current_conversation.connection.channel.add_to_outbox(message)
        self.current_conversation.add_message('{0:%Y-%m-%d %H:%M:%S} : {1} : {2}'.format(message.time,
                                                                                         self.connection_handler.my_name,
                                                                                         message.text))
        self.conversation_messages_model.setStringList(self.current_conversation.get_message_list())
        self.conversation_input.clear()

        # formatted_message = self.format_message(message_text)
        # self.current_conversation.add_message(formatted_message)
        # self.conversation_messages_model.setStringList(self.current_conversation.get_message_list())
        # self.conversation_input.clear()

    def clear_view(self):
        self.contact_name_label.clear()
        self.conversation_messages_model.setStringList([])
        self.conversation_input.clear()

    @pyqtSlot(ConversationContext)
    def change_conversation_context(self, connection_request: ConversationContext):
        self.set_conversation_view()
        self.clear_view()
        self.current_conversation = connection_request
        self.contact_name_label.setText(self.current_conversation.get_contact_name())
        self.conversation_messages_model.setStringList(self.current_conversation.get_message_list())

    @staticmethod
    def format_message(input_text):
        current_time = dt.datetime.now()
        return '{0:%Y-%m-%d %H:%M:%S} : {1}'.format(current_time, input_text)

    def set_default_view(self):
        self.setCurrentIndex(0)

    def set_conversation_view(self):
        self.setCurrentIndex(1)
