import logging

from PyQt5 import QtGui
import threading

from PyQt5.QtCore import QSize
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import qApp, QMainWindow, QAction, QMenuBar, QWidget, QHBoxLayout

from shinycake.core.BackToFrontMerger import back_to_front_merger
from shinycake.core.ConnectionHandler import ConnectionHandler
from shinycake.ui.ConversationPane import ConversationPane
from shinycake.core import UserData
from shinycake.ui.ContactsPane import ContactsPane

from backend.connection import connection_listener



class MainWindow(QMainWindow):
    logger = logging.getLogger(__name__)

    def __init__(self, user_data: UserData):
        super().__init__()

        # Set up the backend
        my_name = user_data.get_name()
        my_ip = user_data.get_ip()
        self.connection_handler = ConnectionHandler(my_name, my_ip)

        self.listener_thread = threading.Thread(target=connection_listener, args=(my_name, my_ip, 11500, self.connection_handler))
        self.listener_thread.start()

        MainWindow.logger.info('Application is starting')

        self.user_data: UserData = user_data
        MainWindow.logger.info(f'User Data: {self.user_data}')

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('Shiny Cake')

        exit_act = QAction(QIcon('shinycake/ui/ic_exit_to_app_3x.png'), '&Exit', self)
        exit_act.setShortcut('Ctrl+Q')
        exit_act.setStatusTip('Exit application')
        exit_act.triggered.connect(qApp.quit)

        menu_bar = QMenuBar()
        self.setMenuBar(menu_bar)

        file_menu = menu_bar.addMenu('&File')
        file_menu.addAction(exit_act)

        central_layout = QHBoxLayout()
        central_layout.setContentsMargins(8, 8, 8, 8)

        contacts_pane = ContactsPane(self.connection_handler)
        conversation_pane = ConversationPane(self.connection_handler)

        contacts_pane.contact_selected.connect(conversation_pane.change_conversation_context)

        central_layout.addWidget(contacts_pane)
        central_layout.addWidget(conversation_pane)

        central_widget = QWidget()
        central_widget.setLayout(central_layout)

        self.setCentralWidget(central_widget)

        self.statusBar().showMessage('Ready')

        self.show()

        self.reader_thread = threading.Thread(target=back_to_front_merger, args=(self.connection_handler, contacts_pane, conversation_pane))
        self.reader_thread.start()


    def closeEvent(self, a0: QtGui.QCloseEvent) -> None:
        MainWindow.logger.info('Application is terminating')
        self.connection_handler.terminate()
        super().closeEvent(a0)



