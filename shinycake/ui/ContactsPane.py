from PyQt5.QtCore import QSize, QModelIndex, Qt, QVariant, pyqtSignal
from PyQt5.QtWidgets import QWidget, QPushButton, QVBoxLayout, QListView, QHBoxLayout, QSpacerItem, QSizePolicy

from shinycake.core.ConversationContext import ConversationContext
from shinycake.core.ConnectionRequestHandler import ConnectionRequestHandler
from shinycake.core.ContactsListModel import ContactsListModel
from shinycake.ui.ConversationInitiationDialog import ConversationInitiationDialog


class ContactsPane(QWidget):

    contact_selected = pyqtSignal(ConversationContext)

    def __init__(self, connection_handler):
        super().__init__()

        self.connection_handler = connection_handler

        self.connection_request_handler = ConnectionRequestHandler()

        self.add_contact_button = QPushButton()
        self.add_contact_button.setObjectName("add_contact_button")
        self.add_contact_button.setText("Add Contact")
        self.add_contact_button.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        self.add_contact_button.setFixedHeight(30)
        self.add_contact_button.clicked.connect(self.initiate_conversation)

        self.add_contact_button_layout = QHBoxLayout()
        self.add_contact_button_layout.addSpacing(20)
        self.add_contact_button_layout.addWidget(self.add_contact_button)
        self.add_contact_button_layout.addSpacing(20)

        add_contact_widget = QWidget(self)
        add_contact_widget.setObjectName('add_contact_widget')
        add_contact_widget.setFixedSize(300, 50)
        add_contact_widget.setLayout(self.add_contact_button_layout)

        self.contacts_list_model = ContactsListModel()

        self.contacts_list_view = QListView()
        self.contacts_list_view.setSizePolicy(QSizePolicy(QSizePolicy.Fixed, QSizePolicy.MinimumExpanding))
        self.contacts_list_view.setFixedSize(300, 600)
        self.contacts_list_view.setDragEnabled(False)
        self.contacts_list_view.setViewMode(QListView.ListMode)
        self.contacts_list_view.setFlow(QListView.TopToBottom)
        self.contacts_list_view.setIconSize(QSize(50, 50))
        self.contacts_list_view.setModel(self.contacts_list_model)
        self.contacts_list_view.setStyleSheet("alternate-background-color: yellow;")
        self.contacts_list_view.doubleClicked.connect(self.conversation_selected)

        layout = QVBoxLayout(self)
        layout.addWidget(add_contact_widget)
        layout.addWidget(self.contacts_list_view)

        layout.setContentsMargins(0, 0, 0, 0)

        self.setLayout(layout)

    def add_conversation(self, conversation_context):
        self.contacts_list_model.add_conversation(conversation_context)

    def initiate_conversation(self):

        conversation_initiation_dialog = ConversationInitiationDialog(self.connection_handler)
        dialog_code = conversation_initiation_dialog.exec()

        # if dialog_code == ConversationInitiationDialog.Accepted:
        #     connection_request = conversation_initiation_dialog.get_connection()
        #     self.conversation_list_model.add_conversation(connection_request)
        # else:
        #     print("We do nothing")

    def conversation_selected(self, index: QModelIndex):
        selected_item: ConversationContext = self.contacts_list_model.data(index, Qt.EditRole)
        self.contact_selected.emit(selected_item)
