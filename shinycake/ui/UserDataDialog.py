from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtWidgets import QDialog, QLineEdit, QPushButton, QLabel, QHBoxLayout, QVBoxLayout, QWidget

from shinycake.core.UserData import UserData


class UserDataDialog(QDialog):

    def __init__(self):
        super().__init__()
        self.setObjectName('user_data_dialog')

        self.setWindowTitle("User Data")
        self.setMinimumWidth(400)

        self.user_name_label = QLabel("Name")
        self.user_name_label.setObjectName('contact_name_label')

        self.user_name_line = QLineEdit()
        self.user_name_validator = QRegExpValidator(QRegExp("\\w{3,256}"))
        self.user_name_line.setValidator(self.user_name_validator)
        self.user_name_line.textChanged.connect(self.check_editing)

        self.ip_address_label = QLabel("IP Address")
        self.ip_address_label.setObjectName('ip_address_label')

        self.ip_address_line = QLineEdit()
        ip_range = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])"  # Part of the regular expression
        ip_regex = QRegExp("^" + ip_range + "\\." + ip_range + "\\." + ip_range + "\\." + ip_range + "$")
        ip_validator = QRegExpValidator(ip_regex, self)
        self.ip_address_line.setValidator(ip_validator)
        self.ip_address_line.textChanged.connect(self.check_editing)

        self.store_user_data_button = QPushButton()
        self.store_user_data_button.setText("Initiate")
        self.store_user_data_button.setEnabled(False)
        self.store_user_data_button.released.connect(self.store_user_data)

        self.cancel_button = QPushButton()
        self.cancel_button.setText("Cancel")
        self.cancel_button.released.connect(self.reject)

        buttons_layout = QHBoxLayout()
        buttons_layout.addWidget(self.store_user_data_button)
        buttons_layout.addWidget(self.cancel_button)
        buttons_widget = QWidget(self)
        buttons_widget.setLayout(buttons_layout)

        self.dialog_layout = QVBoxLayout(self)
        self.dialog_layout.addWidget(self.user_name_label)
        self.dialog_layout.addWidget(self.user_name_line)
        self.dialog_layout.addWidget(self.ip_address_label)
        self.dialog_layout.addWidget(self.ip_address_line)
        self.dialog_layout.addWidget(buttons_widget)

    def check_editing(self, new_text):
        user_name_is_valid = validate_line_edit(self.user_name_line)
        ip_address_is_valid = validate_line_edit(self.ip_address_line)

        self.store_user_data_button.setEnabled(user_name_is_valid and ip_address_is_valid)

    def store_user_data(self):
        self.user_data = UserData(self.user_name_line.text(), self.ip_address_line.text())
        self.accept()

    def get_user_data(self):
        return self.user_data


def validate_line_edit(line_edit):
    if line_edit.hasAcceptableInput():
        line_edit.setStyleSheet("color: black;")
    else:
        line_edit.setStyleSheet("color: red;")

    return line_edit.hasAcceptableInput()
