from PyQt5.QtCore import QRegExp, QObject
from PyQt5.QtGui import QRegExpValidator, QIntValidator
from PyQt5.QtWidgets import QDialog, QLineEdit, QPushButton, QLabel, QHBoxLayout, QVBoxLayout, QWidget

from backend.connection import initiate_connection
from shinycake.core.ConnectionHandler import ConnectionHandler


class ConversationInitiationDialog(QDialog):

    connection_request = None

    def __init__(self, connection_handler: ConnectionHandler):
        super().__init__()
        self.setObjectName('conversation_initiation_dialog')

        self.connection_handler = connection_handler

        self.setWindowTitle("Initiate Conversation")
        self.setMinimumWidth(400)

        self.contact_name_label = QLabel("Name")
        self.contact_name_label.setObjectName('contact_name_label')

        self.contact_name_line = QLineEdit()
        self.contact_name_validator = QRegExpValidator(QRegExp("\\w{3,256}"))
        self.contact_name_line.setValidator(self.contact_name_validator)
        self.contact_name_line.textChanged.connect(self.check_editing)

        self.ip_address_label = QLabel("IP Address")
        self.ip_address_label.setObjectName('ip_address_label')

        self.ip_address_line = QLineEdit()
        ip_range = "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])"  # Part of the regular expression
        ip_regex = QRegExp("^" + ip_range + "\\." + ip_range + "\\." + ip_range + "\\." + ip_range + "$")
        ip_validator = QRegExpValidator(ip_regex, self)
        self.ip_address_line.setValidator(ip_validator)
        self.ip_address_line.textChanged.connect(self.check_editing)

        self.connect_button = QPushButton()
        self.connect_button.setText("Connect")
        self.connect_button.setEnabled(False)
        self.connect_button.released.connect(self.create_connection)

        self.cancel_button = QPushButton()
        self.cancel_button.setText("Cancel")
        self.cancel_button.released.connect(self.reject)

        buttons_layout = QHBoxLayout()
        buttons_layout.addWidget(self.connect_button)
        buttons_layout.addWidget(self.cancel_button)
        buttons_widget = QWidget(self)
        buttons_widget.setLayout(buttons_layout)

        self.dialog_layout = QVBoxLayout(self)
        # self.dialog_layout.addWidget(self.contact_name_label)
        # self.dialog_layout.addWidget(self.contact_name_line)
        self.dialog_layout.addWidget(self.ip_address_label)
        self.dialog_layout.addWidget(self.ip_address_line)
        self.dialog_layout.addWidget(buttons_widget)

    def check_editing(self, new_text):
        # contact_is_valid = validate_line_edit(self.contact_name_line)
        ip_address_is_valid = validate_line_edit(self.ip_address_line)

        # self.connect_button.setEnabled(contact_is_valid and ip_address_is_valid)
        self.connect_button.setEnabled(ip_address_is_valid)

    def create_connection(self):
        initiate_connection(self.connection_handler.my_name,
                            self.connection_handler.my_ip,
                            self.connection_handler.initiate_port,
                            self.ip_address_line.text(),
                            11500,
                            self.connection_handler)
        self.connection_handler.initiate_port += 2
        # self.connection_request = self.connection_request_handler.request_connection(self.ip_address_line.text(),
        #                                                                              self.contact_name_line.text())
        self.accept()

    def get_connection(self):
        return self.connection_request


def validate_line_edit(line_edit):
    if line_edit.hasAcceptableInput():
        line_edit.setStyleSheet("color: black;")
    else:
        line_edit.setStyleSheet("color: red;")

    return line_edit.hasAcceptableInput()
