import logging
import sys

from PyQt5.QtCore import QFile
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication

from shinycake.ui.MainWindow import MainWindow
from shinycake.ui.UserDataDialog import UserDataDialog

if __name__ == '__main__':

    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('shinycake/ui/ic_cake_3x.png'))

    file = QFile("shinycake/ui/stylesheet.css")
    if file.open(QFile.ReadOnly):
        stylesheet = str(file.readAll(), encoding='ascii')
        app.setStyleSheet(stylesheet)

    user_data_dialog = UserDataDialog()
    dialog_code = user_data_dialog.exec()

    if dialog_code == UserDataDialog.Accepted:
        user_data = user_data_dialog.get_user_data()

        window = MainWindow(user_data)
        sys.exit(app.exec())

    else:
        MainWindow.logger.error('No user data to start application')

